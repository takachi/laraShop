-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 15, 2017 at 11:59 AM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `larashop`
--

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at_ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at_ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `name`, `created_at_ip`, `updated_at_ip`, `created_at`, `updated_at`) VALUES
(1, 'ACNE', NULL, NULL, NULL, NULL),
(2, 'RONHILL', NULL, NULL, NULL, NULL),
(3, 'ALBIRO', NULL, NULL, NULL, NULL),
(4, 'ODDMOLLY', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at_ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at_ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at_ip`, `updated_at_ip`, `created_at`, `updated_at`) VALUES
(1, 'MENS', NULL, NULL, NULL, NULL),
(2, 'WOMENS', NULL, NULL, NULL, NULL),
(3, 'KIDS', NULL, NULL, NULL, NULL),
(4, 'FASHION', NULL, NULL, NULL, NULL),
(5, 'CLOTHING', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `depts`
--

CREATE TABLE `depts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `drinks`
--

CREATE TABLE `drinks` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comments` text COLLATE utf8mb4_unicode_ci,
  `rating` int(11) NOT NULL,
  `brew_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `drinks`
--

INSERT INTO `drinks` (`id`, `name`, `comments`, `rating`, `brew_date`, `created_at`, `updated_at`) VALUES
(1, 'Vodka', 'Blood of creativity', 9, '1973-09-03', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dept_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `name`, `email`, `contact_number`, `gender`, `dept_id`, `created_at`, `updated_at`) VALUES
(1, 'Grace Smitham DDS', 'cecelia69@reichert.com', '807-400-1042', '', 0, NULL, NULL),
(2, 'Miss Hortense Reinger Jr.', 'calista05@gmail.com', '421.768.0786 x4736', '', 0, NULL, NULL),
(3, 'Vidal Blick', 'junius52@gmail.com', '765.876.1080', '', 0, NULL, NULL),
(4, 'Ronny Weimann', 'rath.raleigh@cummings.biz', '+1.549.459.6203', '', 0, NULL, NULL),
(5, 'Marion Roberts', 'vesta.wisozk@yahoo.com', '(961) 299-8261', '', 0, NULL, NULL),
(6, 'Antonina Blanda', 'princess57@wolff.com', '(614) 305-8100 x874', '', 0, NULL, NULL),
(7, 'Mr. Kenton Purdy Sr.', 'gwen16@orn.com', '1-470-280-4642 x3965', '', 0, NULL, NULL),
(8, 'Ofelia Flatley', 'joshua24@nienow.biz', '+1.674.852.3533', '', 0, NULL, NULL),
(9, 'Garfield Bechtelar', 'annabell48@mueller.net', '+15018124312', '', 0, NULL, NULL),
(10, 'Mr. Antwan Spencer V', 'cole48@gmail.com', '225.273.3771 x4147', '', 0, NULL, NULL),
(11, 'Margarita Carter', 'nedra.mueller@hotmail.com', '1-258-434-0497 x57039', '', 0, NULL, NULL),
(12, 'Gerson Hand', 'boris.mccullough@yahoo.com', '+17072150785', '', 0, NULL, NULL),
(13, 'German Blanda', 'jailyn.lubowitz@mraz.com', '+18438210888', '', 0, NULL, NULL),
(14, 'Fern Ernser PhD', 'derick59@gmail.com', '(969) 621-5163 x190', '', 0, NULL, NULL),
(15, 'Henderson Lubowitz', 'sgreenholt@steuber.org', '+1 (243) 827-4493', '', 0, NULL, NULL),
(16, 'Kody Corwin', 'yhermiston@gmail.com', '721-249-4744', '', 0, NULL, NULL),
(17, 'Maximo Schinner', 'merritt.kunze@hotmail.com', '+1-790-393-9651', '', 0, NULL, NULL),
(18, 'Era Price', 'xhamill@gmail.com', '+1 (530) 396-2106', '', 0, NULL, NULL),
(19, 'Rowan Beer', 'xcarter@gmail.com', '(864) 935-3047 x426', '', 0, NULL, NULL),
(20, 'Amaya Tromp Sr.', 'dschmeler@senger.info', '(690) 933-3557 x8561', '', 0, NULL, NULL),
(21, 'Jake Friesen IV', 'larkin.quinten@altenwerth.org', '406.698.4672 x7877', '', 0, NULL, NULL),
(22, 'Lavon Stanton', 'gutkowski.julio@corkery.com', '(492) 393-8732 x00744', '', 0, NULL, NULL),
(23, 'Joyce Wehner', 'ferry.thalia@yahoo.com', '223.372.2051 x24682', '', 0, NULL, NULL),
(24, 'Toby Wisozk IV', 'wiegand.veronica@hotmail.com', '707-878-9228', '', 0, NULL, NULL),
(25, 'Ms. Arlene Pollich DVM', 'dolores.herman@rutherford.info', '642.751.8404 x02550', '', 0, NULL, NULL),
(26, 'Dr. Antonietta Veum IV', 'edgar.smitham@yahoo.com', '1-749-455-8484 x9040', '', 0, NULL, NULL),
(27, 'Prof. Bernhard Goyette Sr.', 'elisa61@hotmail.com', '(874) 816-9605', '', 0, NULL, NULL),
(28, 'Dr. Devyn Mraz', 'juwan67@gmail.com', '1-830-674-8156 x83068', '', 0, NULL, NULL),
(29, 'Dr. Garret Will', 'ozella35@yahoo.com', '+18823919065', '', 0, NULL, NULL),
(30, 'Justina Ebert', 'torphy.mortimer@vonrueden.com', '(570) 820-2483', '', 0, NULL, NULL),
(31, 'Adelia Williamson', 'tolson@hermann.net', '338.471.8173', '', 0, NULL, NULL),
(32, 'Rhoda McCullough', 'velva46@halvorson.com', '1-913-807-3588', '', 0, NULL, NULL),
(33, 'Royce McKenzie', 'breitenberg.earl@hotmail.com', '+15836747347', '', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_11_11_045315_create_drinks_table', 1),
(4, '2017_11_11_050426_create_employees', 1),
(5, '2017_11_11_051152_add_gender_to_employees', 1),
(6, '2017_11_11_051256_modify_gender_in_employees', 1),
(7, '2017_11_14_095919_create_depts_table', 1),
(8, '2017_11_14_101324_add_dept_id_in_employees', 1),
(9, '2017_11_15_020630_create_posts_table', 1),
(10, '2017_11_15_020635_create_products_table', 1),
(11, '2017_11_15_020639_create_categories_table', 1),
(12, '2017_11_15_020707_create_brands_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(170) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `blog` tinyint(1) NOT NULL,
  `created_at_ip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_at_ip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `created_at_ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at_ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `title`, `description`, `price`, `category_id`, `brand_id`, `created_at_ip`, `updated_at_ip`, `created_at`, `updated_at`) VALUES
(1, 'Mini skirt black edition', 'Mini skirt black edition', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna', 35, 1, 1, NULL, NULL, NULL, NULL),
(2, 'T-shirt blue edition', 'T-shirt blue edition', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna', 64, 2, 3, NULL, NULL, NULL, NULL),
(3, 'Sleeveless Colorblock Scuba', 'Sleeveless Colorblock Scuba', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna', 13, 3, 2, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `brands_name_unique` (`name`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_name_unique` (`name`);

--
-- Indexes for table `depts`
--
ALTER TABLE `depts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `drinks`
--
ALTER TABLE `drinks`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `drinks_name_unique` (`name`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `employees_email_unique` (`email`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_url_unique` (`url`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `products_name_unique` (`name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `depts`
--
ALTER TABLE `depts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `drinks`
--
ALTER TABLE `drinks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
